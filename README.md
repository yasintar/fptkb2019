# SecAODV: Lightweight Authentication for AODV Protocol

> **Kelompok B**<br>
**Rizvi Sofbrina** -  05111640000037<br>
**Yasinta Romadhona** -  05111640000039<br>
**Nida Regita Fauzianti** -  05111640000127<br>
**Aprilia Khairunnisa** -  05111640000136<br>

## Table of Contents
* [SecAODV: Lightweight Authentication for AODV Protocol](#secaodv-lightweight-authentication-for-aodv-protocol)
  - [Table of Content](#table-of-contents)
  -  [1. Konsep](#1-konsep)
        -  [1.1 Deskripsi Paper](#11-deskripsi-paper)
        -  [1.2 Latar Belakang](#12-latar-belakang)
        -  [1.3 Dasar Teori](#13-dasar-teori)
            -  [1.3.1 AODV Routing Protocol](#131-aodv-routing-protocol)
        -  [1.4 Routing Protokol yang Diusulkan](#14-routing-protokol-yang-diusulkan)
            -  [1.4.1 Solusi Cryptosystem Modern](#141-solusi-cryptosystem-modern)
  -  [2.Implementasi](#2-implementasi)
        -  [2.1 Deskripsi Sistem](#21-deskripsi-sistem)
  -  [3.Referensi](#3-referensi)

## 1. Konsep
### 1.1 Deskripsi Paper
Implementasi MAODV didasarkan pada paper berikut :
*  Judul: SecAODV: Lightweight Authentication for AODV Protocol
*  Penulis: J.A.D.C. Anuradha Jayakody, Rohan Samarasinghe, Saluka R. Kodituwakku
*  Sumber: [SecAODV: Lightweight Authentication for AODV Protocol](https://pdfs.semanticscholar.org/139a/d1266c6808be4f49fcf2e03bb0ab9a40ed6d.pdf?_ga=2.113941447.1531929364.1571317093-606506289.1568955613)

### 1.2 Latar Belakang
Wireless sensor network rentan terhadap serangan keamanan. Salah satu protokol routing pada WSN adalah AODV. AODV melakukan pencarian jalur routing ketika adanya permintaan dari source node untuk mengirim pesan ke destination node. Dikarenakan sensor node yang dipasang di lingkungan dapat diakses secara fisik, maka potensi terjadinya serangan lebih tinggi.<br><br>
Oleh karena itu, paper ini mengusulkan pengembangan framework untuk WSN untuk menyediakan solusi keamanan yang efisien dan terdesentralisasi seperti autentikasi dan intregitas yang dapat meningkatkan keamanan di WSN. Keamanan di Wireless Sensor Networks dapat diimplementasikan menggunakan cryptosystem serta menggunakan cryptosystem. Di dalam penelitian, solusi keamanan akan diimplementasikan menggunakan Cryptosystem modern dalam segi *Packet Delivery Ratio, Average End-to-End Delay,* dan *Average Throughput*<br>

### 1.3 Dasar Teori
#### 1.3.1 AODV Routing Protocol
**AODV** (*Ad  Hoc  On-Demand  Distance  Vector*)
merupakan  distance  vector  routing  protocol yang  termasuk dalam klasifikasi reactive routing protocol, yang hanya melakukan request sebuah rute  saat  dibutuhkan.  Protokol  routing  AODV  melakukan  pemeliharaan  rute  selama  masih diperlukan oleh source node. Setiap node juga bertanggung jawab untuk memelihara informasi rute yang telah disimpan di dalam table routing-nya. 

Terdapat 3 pesan utama untuk proses pembentukan jalur routing dan pemeliharaan jalur routing dengan panjang data masing-masing 32 bit, yaitu :
*  Route Request (RREQ) Memiliki format paket sebagai berikut:
    <br>![Example screenshot](./image/rreq.jpg)
    
    Keterangan :
    *   **Type** : Tipe paket (RREQ = 1)
    *   **|J|R|G|D|U|** : Flag yang menunjukkan keadaan paket
    *   **Reserved** : Default 0, diabaikan
    *   **Hop Count** : Jumlah hops dari node asal ke node yang sedang meng-handle RREQ saat ini
    *   **RREQ ID** : Nomor unik untuk mengidentifikasi RREQ tertentu
    *   **Destination IP Address** : Alamat IP tujuan
    *   **Destination Sequence Number** : Urutan node tujuan
    *   **Originator IP Address** : Alamat IP asal yang menginisialisasi RREQ
    *   **Originator Sequence Number** : Urutan node asal
   <br> 
*  Route Reply (RREP) Memiliki format paket sebagai berikut:
    <br>![Example screenshot](./image/rrep.jpg)

*  Route Error (RERR) Memiliki format paket sebagai berikut:
    <br>![Example screenshot](./image/rerr.jpg)

**Cara Kerja AODV:**
1.  Ketika node asal ingin berkomunikasi dengan node tujuan yang belum memiliki rute, maka node asal akan mem-broadcast RREQ ke jaringan untuk melakukan route discovery (pencarian rute).
    <br>![Example screenshot](./image/RREQ.jpg)
2.  Ketika node intermediate menerima paket RREQ yang belum pernah diterima sebelumnya (cek RREQ ID), maka ia akan membuat reverse route ke node asal.
3.  Node intermediate akan mengecek alamat tujuan pada paket RREQ. Jika ia belum punya informasi rute ke node tujuan yang dimaksudkan, maka ia akan menambah hop count pada paket dan meneruskan broadcast RREQ ke jaringan. Namun jika sebaliknya, ia akan mengirimkan RREP ke node asal.
    <center><br>![Example screenshot](./image/RREP.jpg)</center>
4.  Ketika node asal menerima RREP, ia akan menyimpan rute tersebut dan memulai transmisi data.
5.  Jika  sebuah  link  ke  hop  berikutnya  tidak  dapat  di  deteksi  dengan  menggunakan  metode penemuan  rute,  maka  link  tersebut  akan  diasumsikan  putus  dan  Route  Error  (RERR)  akan disebarkan ke node tetangganya.
    <br>




**Pemeliharaan Rute (*Route Maintenance*) :**
*  Setiap node pada AODV bertanggung jawab untuk memelihara informasi rute yang telah disimpan di dalam routing table-nya
*  Apabila terjadi perubahan topologi yang mengakibatkan suatu node tidak dapat dituju dengan menggunakan informasi rute yang ada pada routing table, maka sutu node akan mengirim pesan route error (RERR) ke node tetangganya dan node tetangganya akan mengirim kembali RRER yang sama ke node tetangga yang lain, dan begitu seterusnya hingga menuju source node.
*  Setiap node yang memperoleh RERR akan menghapus informasi yang mengalami error di dalam routing table-nya. Kemudian source node akan melakukan *route discovery process* kembali apabila rute tersebut masih diperlukan.

### 1.4 Routing Protokol yang Diusulkan
#### 1.4.1 Solusi Cryptosystem Modern
Cryptosystem modern didasarkan pada pertukaran kunci mekanisme. Ada dua tipe, yaitu, sebagai Symmetric Cryptosystem hanya memiliki satu kunci dan Asymmetric Cryptosystem memiliki kunci yang berpasangan (kunci Privat dan Publik). Kunci Privat dimiliki oleh pengguna sedangkan Kunci Publik dimiliki oleh semua orang. Pembentukan kunci dilakukan oleh suatu algoritma seperti Algoritma Diffie-Hellman.<br><br>
Untuk mencapai tujuan tersebut penulis mengusulkan Algoritma Otentikasi yang dikembangkan berjalan di atas Proses Route Discovery. Algoritma ini berisi tentang usulan untuk mengembangkan algoritma bagaimana proses mengirimkan route request (RREQ), menerima RREQ, mengirimkan balasan(RREP), dan menerima RREP
<br>![Example screenshot](./image/algoritma.PNG)


## 2. Implementasi
### 2.1 Deskripsi Sistem
Implementasi routing protocol AODV dilakukan pada sistem dengan :
*  Sistem Operasi : Linux Ubuntu 16.04
*  Aplikasi yang digunakan :
   * NS-2.35 ([Instalasi](https://docs.google.com/document/d/1cNnfd57Okh_KwuJOJv95aE1d1jWxz2UgYWMCLUgepvU/edit))
* Bahasa C++ dengan memodifikasi algoritma AODV yang sudah ada
* Simulation Environment :
    * TCL script   


## 3. Referensi
*  [A Tutorial on the Implementation of Ad-hoc On Demand Distance Vector (AODV) Protocol in Network Simulator (NS-2)](https://drive.google.com/file/d/0B6aQ8IUEyp5NekhLZWVwV0lRNU0/edit)
*  [Ad hoc On-Demand Distance Vector (AODV) Routing](https://www.ietf.org/rfc/rfc3561.txt)
    